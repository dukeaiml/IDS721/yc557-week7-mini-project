# Week 7 Mini Project
> Oliver Chen (yc557)

This project deals with data processing with vector database. The main goal is to ingest data into Vector database, perform queries and aggregations, and visualize the output.

## Project Setup
1. Create a new Rust project.
```bash
cargo new yc557-week7-mini-project
```
2. Add the required dependencies to `Cargo.toml` file.
```toml
[dependencies]
qdrant-client = "1.8.0"
tokio = { version = "1.36.0", features = ["rt-multi-thread"] }
serde_json = "1.0.114"
tonic = "0.11.0"
anyhow = "1.0.81"
```
3. For the vector database, I plan to use Qdrant and followed the official documentation [here](https://qdrant.tech/documentation/quick-start/).
- Download the latest Qdrant image from Dockerhub:
```bash
docker pull qdrant/qdrant
```
- Then run the service:
```bash
docker run -p 6333:6333 -p 6334:6334 \
    -e QDRANT__SERVICE__GRPC_PORT="6334" \
    qdrant/qdrant
```

## Data Ingestion
In `main.rs`, I implement the data ingestion in the following way:

1. **Initializing Qdrant Client**: The `initialize_qdrant_client` function sets up the Qdrant client by creating a configuration based on the URL where the Qdrant server is running.

2. **Creating a Collection**: The `create_collection` function deletes the specified collection if it already exists and then creates a new collection named "test_collection" with the desired configuration. In this case, the collection is configured with vector parameters such as size and distance metric.

3. **Ingesting Vectors**: The `ingest_vectors` function creates ten vectors, each with an associated payload representing a city name. The payload can be some information related to that city, for example, geographical location, population, crime rate, etc. These vectors are then upserted into the "test_collection" using the `upsert_points_blocking` function, which inserts or updates points in the collection.

Here is an example of the vector and the payload:
```rust
PointStruct::new(
    1,
    vec![0.05, 0.61, 0.76, 0.74],
    json!({"city": "Berlin"}).try_into().unwrap(),
),
```

## Query Functionality
After ingesting the data, we can test the vector database by running some test queries.

I first ran a query that was executed to search for vectors similar to a given query vector `[0.2, 0.1, 0.9, 0.7]`. It retrieves up to three closest matches to this query vector from the collection. Here is the query code:

```rust
let search_result = client
    .search_points(&SearchPoints {
        collection_name: collection_name.into(),
        vector: vec![0.2, 0.1, 0.9, 0.7],
        limit: 3,
        with_payload: Some(true.into()),
        ..Default::default()
    })
    .await
    .unwrap();
```

Then, I ran another query with a simple filter that checks if the previous query result has a city name of "New York". Here is the code snippet:

```rust
let search_result = client
    .search_points(&SearchPoints {
        collection_name: collection_name.into(),
        vector: vec![0.2, 0.1, 0.9, 0.7],
        filter: Some(Filter::all([Condition::matches(
            "city",
            "New York".to_string(),
        )])),
        limit: 2,
        with_payload: Some(true.into()),
        ..Default::default()
    })
    .await
    .unwrap();
```

## Visualization

### Database Connection
![db](./images/db.png)

## Query 1 Results
![query 1](./images/test1.png)

## Query 2 Results
![query 2](./images/test2.png)

## Database Logs After Querying
![db-after](./images/db-after.png)